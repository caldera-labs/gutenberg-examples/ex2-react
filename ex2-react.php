<?php
/**
 * Plugin Name:     Learn Gutenberg Example: Using React
 * Plugin URI:      https://learn.wordpress.org
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     learn-gutenberg
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 */
include_once  __DIR__ . '/blocks/blocks.php';

